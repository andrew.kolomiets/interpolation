﻿// lab_5_.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

//libraries of C++

#include<iostream>
#include <string>
#include <math.h>
#include <fstream>

using namespace std;

//define block

#define type  long double
#define line cout<<endl<<"_____________________________________________________"<<endl;

// general function block

void null_vector(type* vector, int size)
{
	for (int i = 0; i < size; i++)
	{
		vector[i] = 0;
	}
}

void output_vector(type* vector, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << vector[i] << endl;
	}
}

type* copy_vector(type* vector_in, type* vector_out, int size)
{
	for (int i = 0; i < size; i++)
	{
		vector_out[i] = vector_in[i];
	}

	return vector_out;
}

void  null_matrix(type** matrix, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			matrix[i][j] = 0;
		}
	}
}

void  output_matrix(type** matrix, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			cout << matrix[i][j] << "\t";
		}
		cout << endl;
	}
}

type** copy_matrix(type** matrix_in, type** matrix_out, int size)
{
	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			matrix_out[i][j] = matrix_in[i][j];
		}
	}
	return matrix_out;
}

void output_paralel_vector(type* vector_first, type* vector_second, int size)
{
	for (int i = 0; i < size; i++)
	{
		cout << vector_first[i]<<"\t"<< vector_second[i]<< endl;
	}
}

type function(type value)
{
	return value * sqrt(value);
}

void data_table(type* vector_value, type* vector_result, type interval, int size)
{
	null_vector(vector_value, size);
	null_vector(vector_result, size);

	type temp_interval = interval / (size-1);

	for (int i = 0; i < size; i++)
	{
		switch (i)
		{
		case 0:
		{
			vector_value[0] = 0;
			break;
		}
		case 1:
		{
			vector_value[1] = temp_interval;
			break;
		}
		
		default:
			vector_value[i] = vector_value[i-1]+temp_interval;
			break;
		}

	}

	for (int i = 0; i < size; i++)
	{
		vector_result[i] = function(vector_value[i]);
	}
}

//lagrange interpolation algorithm

string image_of_polynom_lagrange(type* value, type* result, int size)
{
	string image_of_polynom = " ";

	

	for (int i = 0; i < size; i++)
	{
		string temp_first = " ";
		string temp_second = " ";

		string L=" ";

		for (int j = 0; j < size; j++)
		{
			if (j != i)
			{
				temp_first = temp_first + "(x - " + to_string(value[j]) + ")";
				temp_second = temp_second + "(" + to_string(value[i]) + " - " + to_string(value[j]) + ")";
			}
		}

		L = "("+temp_first + "/" + temp_second+")";

		image_of_polynom = image_of_polynom+"\n"+"+" +L +"*"+ to_string(result[i])+"+";
	}

	return image_of_polynom;
}

type lagrange(type* value, type* result, int size, type choose_elements)
{
	
	type result_of_lagrange_polynom_on_current_element = 0.0;

	
	for (int i = 0; i <size; i++)
	{
		type L = 1.0;

		for (int j = 0; j <size; j++)
		{
			if (j != i)
			{
				L *= (choose_elements - value[j]) / (value[i] - value[j]);
			}
		}

		result_of_lagrange_polynom_on_current_element= result_of_lagrange_polynom_on_current_element+L * result[i];
	}

	return result_of_lagrange_polynom_on_current_element;
}


//splain interpolation algorithm

type * method_Gause(type **matrix, type *vector, int size)
{
	type *solve, max;
	int m, index; m = 0;

	const type epsilone = 0.0001;

	solve = new type[size];

	while (m < size)
	{
		// Поиск строки с максимальным a[i][k]
		max = abs(matrix[m][m]);
		index = m;
		for (int i = m + 1; i < size; i++)
		{
			if (abs(matrix[i][m]) > max)
			{
				max = abs(matrix[i][m]);
				index = i;
			}
		}
		// Перестановка строк

		if (max < epsilone)
		{
			// нет ненулевых диагональных элементов
			cout << "The solution cannot be obtained, because of the zero column:";
			cout << index << "Matrix" << endl;
			return 0;
		}
		for (int j = 0; j < size; j++)
		{
			type temp = matrix[m][j];
			matrix[m][j] = matrix[index][j];
			matrix[index][j] = temp;
		}
		type temp = vector[m];
		vector[m] = vector[index];
		vector[index] = temp;

		// Нормализация уравнений
		for (int i = m; i < size; i++)
		{
			type temp = matrix[i][m];
			if (abs(temp) < epsilone) continue; // для нулевого коэффициента пропустить
			for (int j = 0; j < size; j++)
				matrix[i][j] = matrix[i][j] / temp;
			vector[i] = vector[i] / temp;
			if (i == m)  continue; // уравнение не вычитать само из себя
			for (int j = 0; j < size; j++)
				matrix[i][j] = matrix[i][j] - matrix[m][j];
			vector[i] = vector[i] - vector[m];
		}
		m++;
	}
	// обратная подстановка
	for (m = size - 1; m >= 0; m--)
	{
		solve[m] = vector[m];
		for (int i = 0; i < m; i++)
			vector[i] = vector[i] - matrix[i][m] * solve[m];
	}
	return solve;
}

void coefitient_splain(type* vector_result, type* vector_value,type** coeficient_matrix,int size)
{
	//data block
	int space = size - 1;

	//creation block
	type* a = new type[space];             null_vector(a, space);
	type* b = new type[space];             null_vector(b, space);
	type* c = new type[space];             null_vector(c, space);
	type* d = new type[space];             null_vector(d, space);
	type* h = new type[space];             null_vector(h, space);
	type*right_vector = new type[space];   null_vector(right_vector, space);

	type** matrix = new type*[space];

	for (int i = 0; i < space; i++)
	{
		matrix[i] = new type[space];
	}

	type** matrix_coeficient = new type*[space];

	for (int i = 0; i < space; i++)
	{
		matrix_coeficient[i] = new type[4];
	}

	null_matrix(matrix, space);

	for (int i = 0; i < space; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			matrix[i][j] = 0;
		}
	}

	////// interpolation algorithm block start //////

	for (int i = 0; i < space; i++) //exelent
	{
		a[i] = vector_result[i];
		h[i] = vector_value[i + 1] - vector_value[i];
	}

	cout << endl << "-coeficient A of splain polynoms:" << endl;
	output_vector(a, space);
	cout << endl << "-coeficient h of splain polynoms:" << endl;
	output_vector(h, space);


	right_vector[0] = 0; right_vector[space - 1] = 0;

	for (int i = 1; i < space - 1; i++)
	{
		right_vector[i] = 3 * (((vector_result[i + 1] - vector_result[i]) / h[i + 1]) - ((vector_result[i] - vector_result[i - 1]) / h[i]));
	}

	cout << endl << "-right vector of system, which we must solve:" << endl;
	output_vector(right_vector, space);

	for (int i = 0; i < space; i++)
	{
		for (int j = 0; j < space; j++)
		{
			if (i == j && i > 0 && i < space - 1)
			{
				matrix[i][j - 1] = h[i];
				matrix[i][j] = 2 * (h[i] + h[i + 1]);
				matrix[i][j + 1] = h[i + 1];
			}
			if (i == j && i == 0)
			{
				matrix[i][j] = 2 * (h[i] + h[i + 1]);
				matrix[i][j + 1] = h[i + 1];
			}
			if (i == j && i == space - 1)
			{
				matrix[i][j] = 2 * (h[i - 1] + h[i]);
				matrix[i][j - 1] = h[i];
			}
		}
	}

	cout << endl << "-matrix of system, which we must solve:" << endl;
	output_matrix(matrix, space);

	cout << endl << "-solved system:" << endl;
	output_vector(method_Gause(matrix, right_vector, space), space);

	copy_vector(method_Gause(matrix, right_vector, space), c, space);

	d[space - 1] = -(c[space - 1]) / (3 * h[space - 1]);
	b[space - 1] = ((vector_result[space - 1] - vector_result[space - 2]) / h[space - 1]) - (2 * h[space - 1] * c[space - 1] / 3);

	for (int i = 0; i < space; i++)
	{
		if (i != space - 1)
		{
			d[i] = (c[i + 1] - c[i]) / (3 * h[i]);
			b[i] = ((vector_result[i + 1] - vector_result[i]) / h[i]) - (h[i] * (c[i + 1] + 2 * c[i]) / 3);
		}

		matrix_coeficient[i][0] = a[i];
		matrix_coeficient[i][1] = b[i];
		matrix_coeficient[i][2] = c[i];
		matrix_coeficient[i][3] = d[i];
	}

	cout << endl << "-coeficient of all spline function:" << endl;

	for (int i = 0; i < space; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			cout << "\t" << matrix_coeficient[i][j];
			coeficient_matrix[i][j] = matrix_coeficient[i][j];
		}
		cout << endl;
	}


	////// interpolation algorithm block end //////

	//deliting block

	for (int i = 0; i < space; i++)
	{
		delete matrix_coeficient[i];
	}

	for (int i = 0; i < space; i++)
	{
		delete matrix[i];
	}

	delete[] matrix_coeficient;
	delete[] matrix;
	delete[] right_vector;
	delete[] h;
	delete[] d;
	delete[] c;
	delete[] b;
	delete[] a;

}

type result_of_splain_interpolation(type** matrix_coeficient, type*vector_value, type variable,int size)
{

	int i = 0;

	while (vector_value[i] < variable&&i<size-1) //old vatiable -1
	{
		i++;
	}

	return matrix_coeficient[i][0] + matrix_coeficient[i][1] * (variable - vector_value[i]) + matrix_coeficient[i][2] * powf((variable - vector_value[i]), 2) + matrix_coeficient[i][3] * pow((variable - vector_value[i]), 3);
}

// main function

int main()
{

	//data block start

	type start = 0;
	type end = 4;

	type value;
	type interval = end - start;

	int number_of_nodes;

	//data block end

	//program start

	line

	cout << endl << "Program start..." << endl;

	line

	cout << endl << "Input number of nodes:" << endl;
	cin >> number_of_nodes;

	type* vector_value = new type[number_of_nodes];
	type*vector_result = new type[number_of_nodes];

	line

	cout << endl << "Find table of values and result of function on curent values:" << endl;

	data_table(vector_value, vector_result, interval, number_of_nodes );

	cout << endl << "value"<<"\t result function" << endl;

	output_paralel_vector(vector_value, vector_result, number_of_nodes);

	line

	cout << endl << "Lagrange interpolation:" << endl;

	cout << endl << "-image of interpolation polynom:" << endl;

	string temp = image_of_polynom_lagrange(vector_value, vector_result, number_of_nodes);

	temp[0] = ' ';
	temp[temp.length() - 1] = ' ';

	cout<<endl<<"Function(x)="<<temp<<endl;

	cout << endl << "-input value, where you want to get result of interpolation:" << endl;
	cin >> value;
	cout << endl << "-result of interpolation:" << lagrange(vector_value, vector_result, number_of_nodes, value) << endl;

	type error_lagrange = 0.0;

	{
	fstream fout;          fout.open("graph.xls", ios::out);

		fout << "\t" << "Value"<<"\t" <<"Error"<<"\t"<<"Interpolation result"<<"\t"<<"Function result"<< endl;

	for (type i = 0; i <= 4.0; i += 0.08)
	{

		fout << "\t" << i << "\t" << abs(lagrange(vector_value, vector_result, number_of_nodes, i) - function(i)) << "\t" << lagrange(vector_value, vector_result, number_of_nodes, i) << "\t" << function(i) << endl;

		if (error_lagrange > abs(lagrange(vector_value, vector_result, number_of_nodes, i) - function(i)))
		{
			continue;
		}
		if (error_lagrange < abs(lagrange(vector_value, vector_result, number_of_nodes, i) - function(i)))
		{
			error_lagrange = abs(lagrange(vector_value, vector_result, number_of_nodes, i) - function(i));
			
		}
	}


	fout.close();

	}

	cout << endl << "-error of interpolation (maximum value of difference root):" << error_lagrange<< endl;
	
	line

	type** matrix_coeficient = new type*[number_of_nodes-1];

	for (int i = 0; i < number_of_nodes-1; i++)
	{
		matrix_coeficient[i] = new type[4];
	}

	cout << endl << "Splain interpolation:" << endl;

	coefitient_splain( vector_result, vector_value, matrix_coeficient, number_of_nodes);
		
	{
		fstream fout;          fout.open("graph_addition.xls", ios::out);

		/*fout << "\t" << "Value"  << "\t" << "Interpolation result" << "\t" << "Function result"<<"\t"<<"Splain inerpolation result"<< endl;

		for (type i = 0; i <4.0; i += 0.08)
		{

			fout << "\t" << i << "\t" << lagrange(vector_value, vector_result, number_of_nodes, i) << "\t" << function(i)<< "\t"<<result_of_splain_interpolation(matrix_coeficient, vector_value, i, number_of_nodes-1) << endl;

			
		}*/

		fout << "\t" << "Value" << "\t" << "Error Lagrange interpolation"<<"\t"<< "Error Spline interpolation" << "\t" << "Interpolation result" << "\t" << "Function result" << endl;

		for (type i = 0; i <= 4.0; i += 0.08)
		{

			fout << "\t" << i << "\t" << abs(lagrange(vector_value, vector_result, number_of_nodes, i) - function(i)) <<"\t"<< abs(result_of_splain_interpolation(matrix_coeficient, vector_value, i, number_of_nodes - 1) - function(i)) << "\t" << result_of_splain_interpolation(matrix_coeficient, vector_value, i, number_of_nodes - 1) << "\t" << function(i) << endl;
	

		
		}




		fout.close();

	}

	for (int i = 0; i < number_of_nodes - 1; i++)
	{
		delete matrix_coeficient[i];
	}

	delete[] matrix_coeficient;

	delete[] vector_result;
	delete[] vector_value;

	line
	cout << endl << "Program end..." << endl;
	line

	//program end


	return 0;
}



// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.


//void solving_matrix(double** TDM, double* F)
//{
//	double* alph = new double[n - 1];
//	double* beta = new double[n - 1];
//
//	int i;
//
//	alph[0] = -TDM[2][0] / TDM[1][0];
//	beta[0] = F[0] / TDM[1][0];
//
//	for (i = 1; i < n - 1; i++)
//	{
//		alph[i] = -TDM[2][i] / (TDM[1][i] + TDM[0][i] * alph[i - 1]);
//		beta[i] = (F[i] - TDM[0][i] * beta[i - 1]) / (TDM[1][i] + TDM[0][i] * alph[i - 1]);
//	}
//	right_vector[n - 1] = (F[n - 1] - TDM[0][n - 1] * beta[n - 2]) / (TDM[1][n - 1] + TDM[0][n - 1] * alph[n - 2]);
//
//	for (i = n - 2; i > -1; i--)
//	{
//		right_vector[i] = right_vector[i + 1] * alph[i] + beta[i];
//	}
//}
